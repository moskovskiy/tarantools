package tarantools

import (
	"fmt"
	"strconv"
	"time"
)

// ParseMap generates dictionary from random map
func ParseMap(input map[Any]Any) (result Dictionary) {
	result = make(Dictionary)
	for key, value := range input {
		strKey := fmt.Sprintf("%v", key)
		strValue := fmt.Sprintf("%v", value)

		result[strKey] = strValue
	}

	return result
}

// GetUnixTime returns timestamp in milliseconds
func GetUnixTime() int {
	return int(time.Now().UnixNano()) / 1000 // / int(time.Millisecond))
}

func minutify(minute int) string {
	if minute < 10 {
		return "0" + strconv.Itoa(minute)
	}
	return strconv.Itoa(minute)
}

// GenerateStringDateFromUnix generates human-readable date from timestamp in milliseconds
func GenerateStringDateFromUnix(unix int) string {
	timeFrom := time.Unix(int64(unix)/1000000, 0)

	monthsNames := []string{"Jan", "Feb", "Mar", "Apr", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
	dayNames := []string{"Mon", "Tue", "Wen", "Thu", "Fri", "Sat", "Sun"}
	month := monthsNames[timeFrom.Month()-2]
	day := timeFrom.Day()
	year := timeFrom.Year()
	hour := timeFrom.Hour()
	minute := timeFrom.Minute()
	weekday := dayNames[timeFrom.Weekday()]

	return weekday + ", " + strconv.Itoa(day) + " " + month + " " + strconv.Itoa(year) + " " + strconv.Itoa(hour) + ":" + minutify(minute)
}
