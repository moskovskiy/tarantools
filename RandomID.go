package tarantools

import (
	"math/rand"
	"time"
	//"log"
)

// RandomID represents random string that can be used as id for Tarantool tuple, made of 16 bytes
type RandomID struct {
	value []byte
}

// All default alphabets are URL acceptable ids
const (
	// IDGenerationAlphabetDefault = 0-9A-Za-z_
	IDGenerationAlphabetDefault = "_0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// IDGenerationAlphabetNoUnderscore = 0-9A-Za-z
	IDGenerationAlphabetNoUnderscore = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// IDGenerationAlphabetLettersOnly = A-Za-z
	IDGenerationAlphabetLettersOnly = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// IDGenerationAlphabetDigitsOnly = 0-9
	IDGenerationAlphabetDigitsOnly = "0123456789"
)

// GenerateTimeBytes generates serial alphabet string from current timestamp
func GenerateTimeBytes(time int, alphabet string) (result string) {
	powd := len(alphabet)

	index := 0
	for {
		rem := time % powd
		time = time / powd
		result = string(alphabet[rem]) + result // append + result

		if (time < 1) || (index > 10) {
			break
		}
		index++
	}

	return result
}

// Initialize random with elements from alphabet
func (s *RandomID) Initialize(alphabet string) {

	b := make([]byte, 16)

	for i := range b {
		rand.Seed(time.Now().UTC().UnixNano())
		b[i] = alphabet[rand.Intn(len(alphabet))]
	}

	//log.Print("Debug> Random_id.go> Generated random sequence: \n")
	//log.Print("Debug> Random_id.go> ", b, "\n")

	s.value = b
}

// GenerateTupleID returns readable string from byte serial, delimeter splits it after every 4 symbols
func (s *RandomID) GenerateTupleID(delimeter string) TupleID {
	var result = string(s.value)
	index := 4
	result = result[:index] + delimeter + result[index:]
	index = 9
	result = result[:index] + delimeter + result[index:]
	index = 14
	result = result[:index] + delimeter + result[index:]

	//log.Print("Debug> Random_id.go> Generated string from rand byte: \n")
	//log.Print("Debug> Random_id.go> random_id = ", s.value, " string = ", result, "\n")

	return result
}
