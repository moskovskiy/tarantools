package tarantools

// tuple is used to store any number of incoming variables and pass them to TarantoolDB
// any is a type of any type
// dictionary is a key-value space, where all keys are strings

// Any types resembles any type, hence any type complies with empty interface
// use Any to send any type ¯\_(ツ)_/¯
type Any = interface{}

// Tuple is an array of any tupes a.k.a array of multiple types
// e.g. [string, bool, int, YourStruct, YourInterface]
type Tuple = []Any

// Dictionary is an array of any types, except where elements are labled with string name (it's a map)
// e.g. ["name":"Alex", "married":false, "age":3, "parent":Parent]
type Dictionary = map[string]Any

// TupleID is just for ease of code reading
type TupleID = string

// TarantoolIndex same
type TarantoolIndex = string
