module gitlab.com/detlax/tarantools

go 1.12

require (
	github.com/tarantool/go-tarantool v0.0.0-20191229181800-f4ece3508d87
	gopkg.in/vmihailenco/msgpack.v2 v2.9.1 // indirect
)
